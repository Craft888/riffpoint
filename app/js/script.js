$(document).ready(function () {
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        if (this.hash) {
            $('body, html').animate({
                scrollTop: $(this.hash).offset().top - $('#header').height()
            }, 500);
        }
    });
    $(document).on('scroll', function (e) {
        if ($(document).scrollTop() >= $('#about').offset().top - 100) {
            $('#go2top').show();
        }
        else {
            $('#go2top').hide();
        }
    });
    $('#go2top').on('click', function () {
        $('body, html').animate({
            scrollTop: 0
        }, 800);
    });

    function slideRight() {
        $('#slide-right').off('click', slideRight);
        $('#slider').animate({
            left: -$('#slider').width()
        }, 500, function () {
            $('.slide').first().appendTo($('#slider'));
            $('#slider').css('left', 0);
            $('#slide-right').on('click', slideRight);
        });
    }

    function slideLeft() {
        $('#slide-left').off('click', slideLeft);
        $('.slide').last().prependTo($('#slider'));
        $('#slider').offset({
            left: -$('#slider').width()
        });
        $('#slider').animate({
            left: 0
        }, 500, function () {
            $('#slide-left').on('click', slideLeft);
        });
    }
    $('#slide-right').on('click', slideRight);
    $('#slide-left').on('click', slideLeft);
});