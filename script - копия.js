$(document).ready(function () {
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $('body').animate({
            scrollTop: $(this.hash).offset().top - $('#header').height()
        }, 500);
    });
    $(document).on('scroll', function (e) {
        if ($(document).scrollTop() >= $('#about').offset().top - 100) {
            $('#go2top').show();
        }
        else {
            $('#go2top').hide();
        }
    });
    $('#go2top').on('click', function () {
        $('body').animate({
            scrollTop: 0
        }, 800);
    });

    function slideRight() {
        $('#slide-right').off('click', slideRight);
        $('#slider').animate({
            left: -$('#slider').width()
        }, 500, function () {
            $('.slide').first().appendTo($('#slider'));
            $('#slider').css('left', 0);
            $('#slide-right').on('click', slideRight);
        });
    }

    function slideLeft() {
        $('#slide-left').off('click', slideLeft);
        $('.slide').last().prependTo($('#slider'));
        $('#slider').offset({
            left: -$('#slider').width()
        });
        $('#slider').animate({
            left: 0
        }, 500, function () {
            $('#slide-left').on('click', slideLeft);
        });
    }
    $('#slide-right').on('click', slideRight);
    $('#slide-left').on('click', slideLeft);
    var distance = 0;
    var target = true;

    function mouseDown(evt) {
        if (!$(evt.target).hasClass('slide')) {
            target = false;
            return;
        }
        target = true;
        $('.slide').last().clone().prependTo($('#slider'));
        $('#slider').offset({
            left: -$('#slider').width()
        });
        distance = 0;
        $('html').on('mousemove', function (e) {
            distance = e.clientX - evt.offsetX;
            $('#slider').offset({
                left: e.pageX - evt.offsetX - $('#slider').width()
            });
        });
    }

    function mouseUp() {
        $('html').off('mousemove');
        if (!target) return;
        if (!distance) {
            $('.slide').first().remove();
            $('#slider').offset({
                left: 0
            });
            return;
        }
//        if (distance > -100 && distance < 100) {
//            $('#slider').off('mousedown', mouseDown);
//            $('#slider').off('mouseup', mouseUp);
//            $('#slider').animate({
//                left: -$('#slider').width()
//            }, 500, function () {
//                $('.slide').first().remove();
//                $('#slider').offset({
//                    left: 0
//                });
//                $('#slider').on('mousedown', mouseDown);
//                $('#slider').on('mouseup', mouseUp);
//            });
//        }
//        else
        if ($('#slider').offset().left > (-$('#slider').width())) {
            console.log('left');
            $('#slider').off('mousedown', mouseDown);
            $('#slider').off('mouseup', mouseUp);
            $('#slider').animate({
                left: 0
            }, 500, function () {
                $('.slide').last().remove();
                $('#slider').on('mousedown', mouseDown);
                $('#slider').on('mouseup', mouseUp);
            });
        }
        else {
            console.log('right');
            $('#slider').off('mousedown', mouseDown);
            $('#slider').off('mouseup', mouseUp);
            $('#slider').animate({
                left: -$('#slider').width() * 2
            }, 500, function () {
                $('.slide').first().remove();
                $('.slide').first().appendTo($('#slider'));
                $('#slider').offset({
                    left: 0
                });
                $('#slider').on('mousedown', mouseDown);
                $('#slider').on('mouseup', mouseUp);
            });
        }
    }
    $('#slider').on('mousedown', mouseDown);
    $('#slider').on('mouseup', mouseUp);
});